import React from 'react';
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import Register from './pages/Register';
import Page404 from './pages/Page404';
import Root from './pages/Root';
import { createTheme, ThemeProvider } from '@mui/material/styles';

export default App;
function App() {

  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route
        path="/"
        element={<Root />}
        errorElement={<Page404 />}
      >
        <Route index element={<HomePage />} />
        <Route errorElement={<Page404 />}>
          <Route
            path="login"
            element={<LoginPage />}
          />
          <Route
            path="register"
            element={<Register />}
          />
        </Route>
      </Route>
    )
  );

  const theme = createTheme({
    palette: {
        primary: {
            main: '#3baa58'
        },
        secondary: {
            main: '#aa3b8e'
        },
    },
});

  return (
    <ThemeProvider theme={theme}>
      <RouterProvider router={router} />
    </ThemeProvider>
  );
}


