import { Box, Button, Typography } from '@mui/material';
import { useAppSelector, useAppDispatch } from '../app/hooks';
import { logout, selectUser } from '../userSlice';
import { useNavigate } from 'react-router-dom';


export default function HomePage() {
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const logoutClick = () => {
        dispatch(logout());
    };
    
    return (
        <>
            <Box sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}>

                <main>
                    <Typography
                        component="h1"
                        variant="h3"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    >
                        Главная страница
                    </Typography>
                    <Typography variant="h5" align="center" color="text.primary" paragraph>
                        Домашнее задание<br />
                        Роутинг и управление стейтом с React
                    </Typography>
                    <Typography variant="h6" align="center" color="text.secondary" paragraph>
                        Здесь демонстрируется вход и регистрация пользователя
                    </Typography>


                    {user.name !== '' &&
                        <>
                            <Typography variant="h5" align="center" color="text.secondary" paragraph>
                                Вы вошли под именем {user.name}
                            </Typography>
                            <Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                                <Button variant="contained" onClick={logoutClick} sx={{ width: 1 / 4, mr: 2 }} >Выйти</Button>
                            </Box>
                        </>
                    }

                    {user.name === '' &&
                        <Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                            <Button variant="contained" onClick={() => { navigate("/login"); }} sx={{ width: 1 / 4, mr: 2 }} >Вход</Button>
                            <Button variant="contained" onClick={() => { navigate("/register"); }} sx={{ width: 1 / 4, mr: 2 }}  >Регистрация</Button>

                        </Box>
                    }

                </main>
            </Box>
        </>
    );
}