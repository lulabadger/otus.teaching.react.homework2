import { TextField } from '@mui/material';

type UserTextFieldProps = {
    inputText: string;
    setInputText(enterNam: string): void,
    error: boolean,
    label: string,
    type: string,
    errorText?:string
};

export default function UserTextField(props: UserTextFieldProps) {
    return (
        <>
            <TextField
                margin="normal"
                fullWidth
                required
                error={props.error}
                type={props.type}
                label={props.label}
                helperText={props.error? props.errorText: null}
                onChange={(e) => { props.setInputText(e.target.value); }}
                value={props.inputText}
                autoComplete='off'
            />
        </>
    );
}