import React, { useState, useEffect } from 'react';
import { Box, Button } from '@mui/material';
import { selectUser, User, login } from '../userSlice';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { Link, useNavigate } from "react-router-dom";

import FormContaner from './components/FormContainer'
import ErrorAlert from './components/ErrorAlert';
import UserTextField from './components/UserTextField';



export default function LoginPage() {

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const user = useAppSelector(selectUser);

    const [inputName, setInputName] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [doRegister, setDoRegister] = useState(false);

    useEffect(() => {
        if (user.name !== '') {
            navigate("/");
        }
    }, [user.name, navigate]);


    const setName = (text: string) => {
        setInputName(text);
        setDoRegister(false);
    }
    const setPassword = (text: string) => {
        setInputPassword(text);
        setDoRegister(false);
    }

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const payload: User = { name: inputName, password: inputPassword };
        dispatch(login(payload));
        setDoRegister(true);
    }

    return (
        <>
            <FormContaner name='Вход' >
                <Box
                    component="form"
                    onSubmit={submitForm}
                    sx={{ mt: 1 }}
                >
                    <UserTextField
                        error={false}
                        inputText={inputName}
                        setInputText={setName}
                        label="Имя пользователя"
                        type='text'
                    />

                    <UserTextField
                        error={false}
                        inputText={inputPassword}
                        setInputText={setPassword}
                        label="Пароль"
                        type='password'
                    />


                    <ErrorAlert error=' Неправильно заданы имя пользователя или пароль. Проверьте ввод'
                        show={doRegister} />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        size="large"
                        sx={{ mt: 2, mb: 2 }}
                    >
                        Войти
                    </Button>

                    <Link to="/register" >
                        Зарегистрироваться?
                    </Link>
                </Box>
            </FormContaner>
        </>
    );
}